# README #
This is a simple project to test the feature of guaranteeing message  processing of Storm. 

The result of this test is:

* If a bolt does not get a tuple for some error(e.g, network error) it cannot produce failed tuples. 

* If a tuple remains within the execute method of a bolt for more than TOPOLOGY_MESSAGE_TIMEOUT_SECS seconds, it is considered as a failed tuple. The tuple is anchored but not acked by the acker process. 

* To throw error and make the tuple as a failed tuple, we need to throw FailedException in the execute method of bolt.

For failed tuples, we can re-emit the tuple from spout. 

ref: https://storm.apache.org/documentation/Guaranteeing-message-processing.html