package replay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class EmittingBolt extends BaseBasicBolt {

	private static final long serialVersionUID = 6302473106812356303L;
	public static Logger LOG = LoggerFactory.getLogger(EmittingBolt.class);

	@Override
	public void execute(Tuple arg0, BasicOutputCollector arg1) {
		LOG.info("In execute method of Emitting-Bolt: " + Thread.currentThread().getId() + " message: " + arg0.getMessageId() + " tuple: " + arg0 );
		arg1.emit(new Values(arg0.getValue(0)));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
