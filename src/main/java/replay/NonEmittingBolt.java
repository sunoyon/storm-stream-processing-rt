package replay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class NonEmittingBolt extends BaseBasicBolt {

	private static final long serialVersionUID = 6372621491589254725L;
	public static Logger LOG = LoggerFactory.getLogger(NonEmittingBolt.class);

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		LOG.info("In execute method of Non-Emitting-Bolt");
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
