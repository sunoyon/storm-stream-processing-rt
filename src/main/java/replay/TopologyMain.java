package replay;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;

public class TopologyMain {

	/*
	 *  All bolts successfully emit tuples.
	 *  Number of emitted tuples in Topology stats is equal to the sum of tuples emitted within the topology.
	 *  Number of acked tuples in Topology stats is equal to the number of acked tuples of spout.
	 *  Often the number may vary from the actual value. We dumped the output tuples in a file. 
	 *  The number of output tuples is equal to the emitted tuples, though these number may vary in Storm UI 
	 */
	
	public static StormTopology completeTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Emit-Bolt", new EmittingBolt()).shuffleGrouping(
				"B-Emit-Bolt");
		builder.setBolt("D-File-Write-Bolt",
				new FileWriterBolt("Complete-Topology")).shuffleGrouping(
				"C-Emit-Bolt");
		return builder.createTopology();
	}
	
	public static StormTopology loopTopology() {
		TopologyBuilder builder = new TopologyBuilder();
		
		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new LoopBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Emit-Bolt", new EmittingBolt()).shuffleGrouping(
				"B-Emit-Bolt");
		builder.setBolt("D-File-Write-Bolt",
				new FileWriterBolt("Complete-Topology")).shuffleGrouping(
						"C-Emit-Bolt");
		return builder.createTopology();
	}

	/*
	 * As the end bolt C-Non-Emit-Bolt acks after the execute method call (though it does not emit tuples) 
	 * there is no failed tuples. The end bolt anchored the tuples and acked after execute method.
	 */
	
	public static StormTopology nonEmittingEndTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Non-Emit-Bolt", new NonEmittingBolt())
				.shuffleGrouping("B-Emit-Bolt");
		return builder.createTopology();
	}

	/*
	 * As the mid bolt C-Non-Emit-Bolt acks after the execute method call (though it does not emit tuples) 
	 * there is no failed tuples. Because C-Non-Emit-Bolt anchored the tuples and acked after execute method.
	 * The next bolt D-Emit-Bolt does not get and anchor any tuple, so there is not failed tuple during its execution 
	 * though the mid bolt should have emitted tuples to it according to the topology definition. 
	 */
	public static StormTopology nonEmittingMidTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Non-Emit-Bolt", new NonEmittingBolt())
				.shuffleGrouping("B-Emit-Bolt");
		builder.setBolt("D-Emit-Bolt", new EmittingBolt()).shuffleGrouping(
				"C-Non-Emit-Bolt");
		builder.setBolt("E-File-Write-Bolt", new FileWriterBolt("Non-Emit-Mid"))
				.shuffleGrouping("D-Emit-Bolt");
		return builder.createTopology();
	}
	
	/* As the mid bolt C-Failed-Bolt explicitly throws FailedException, the tuples are considered to be failed. 
	 * Topology stats shows the number of failed spout tuples.  
	 */
	public static StormTopology failedTopology() {
		TopologyBuilder builder = new TopologyBuilder();
		
		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Failed-Bolt", new FailedBolt())
		.shuffleGrouping("B-Emit-Bolt");
		builder.setBolt("D-Emit-Bolt", new EmittingBolt()).shuffleGrouping(
				"C-Failed-Bolt");
		builder.setBolt("E-File-Write-Bolt", new FileWriterBolt("Non-Emit-Mid"))
		.shuffleGrouping("D-Emit-Bolt");
		return builder.createTopology();
	}
	
	/* As the mid bolt C-Failed-Bolt sleeps 40 sec ( > Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS) in its execution method, the tuples are considered to be failed. 
	 * Topology stats shows the number of failed spout tuples.  
	 * If a tuple is anchored and is not acked it is considered to be failed. So if execute method takes long time, then failed message occurs. 
	 */
	public static StormTopology sleepTopology() {
		TopologyBuilder builder = new TopologyBuilder();
		
		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 2).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Sleep-Bolt", new SleepBolt(), 2)
		.shuffleGrouping("B-Emit-Bolt");
		builder.setBolt("D-Emit-Bolt", new EmittingBolt(), 2).shuffleGrouping(
				"C-Sleep-Bolt");
		builder.setBolt("E-File-Write-Bolt", new FileWriterBolt("Non-Emit-Mid"))
		.shuffleGrouping("D-Emit-Bolt");
		return builder.createTopology();
	}

	/*
	 * As the end bolt E-Non-Emit-Bolt acks after the execute method call (though it does not emit tuples) 
	 * there is no failed tuples. The end bolt anchored the tuples and acked after execute method.
	 */
	public static StormTopology SplitStreamTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Emit-Bolt", new DoubleStreamBolt())
				.shuffleGrouping("B-Emit-Bolt");
		builder.setBolt("D-Emit-Bolt", new EmittingBolt()).shuffleGrouping(
				"C-Emit-Bolt", DoubleStreamBolt.myFirstStream);
		builder.setBolt("E-Non-Emit-Bolt", new NonEmittingBolt())
				.shuffleGrouping("C-Emit-Bolt", DoubleStreamBolt.mySecondStream);
		return builder.createTopology();
	}
	
	/*
	 * E-Emit-Bolt does not get any tuple from D-Non-Emit-Bolt. However, D-Non-Emit-Bolt gets the tuples,
	 * anchors them and acks them after execute method. But it does not emit the tuples.
	 * As the tuples are acked in D-Non-Emit-Bolt, there is no failed tuples.
	 */

	public static StormTopology DagTopology() {
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("A-Spout", new MockSpout(), 1);
		builder.setBolt("B-Emit-Bolt", new EmittingBolt(), 1).shuffleGrouping(
				"A-Spout");
		builder.setBolt("C-Emit-Bolt", new DoubleStreamBolt()).shuffleGrouping(
				"B-Emit-Bolt");
		builder.setBolt("D-Non-Emit-Bolt", new NonEmittingBolt())
				.shuffleGrouping("C-Emit-Bolt", DoubleStreamBolt.myFirstStream);
		builder.setBolt("E-Emit-Bolt", new EmittingBolt())
				.shuffleGrouping("C-Emit-Bolt", DoubleStreamBolt.mySecondStream)
				.shuffleGrouping("D-Non-Emit-Bolt");
		return builder.createTopology();
	}

	public static void main(String[] args) throws AlreadyAliveException,
			InvalidTopologyException, InterruptedException {

		Config conf = new Config();
		conf.setNumAckers(1);
		conf.setDebug(true);
		StormTopology aTopology;
		int choice = 7;
		if (args != null && args.length > 1)
			choice = Integer.parseInt(args[1]);
		switch (choice) {
		case 1:
			aTopology = completeTopology();
			break;
		case 2:
			aTopology = nonEmittingEndTopology();
			break;
		case 3:
			aTopology = nonEmittingMidTopology();
			break;

		case 4:
			aTopology = SplitStreamTopology();
			break;

		case 5:
			aTopology = DagTopology();
			break;
			
		case 6:
			aTopology = failedTopology();
			break;
			
		case 7:
			aTopology = sleepTopology();
			break;

		default:
			aTopology = completeTopology();
			break;
		}

		if (args != null && args.length > 1) {
			conf.setNumWorkers(1);
			StormSubmitter.submitTopology(args[0], conf, aTopology);
		} else {
			conf.setMaxTaskParallelism(3);
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("Sample-Topology-Local", conf, aTopology);
			// Thread.sleep(10000);
			// cluster.shutdown();
		}

	}
}
