package replay;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DoubleStreamBolt extends BaseBasicBolt {

	private static final long serialVersionUID = -8289950400097619642L;
	private int hashIndex = 0;
	public static final String myFirstStream = "STREAM1";
	public static final String mySecondStream = "STREAM2";

	@Override
	public void execute(Tuple arg0, BasicOutputCollector arg1) {
		if (hashIndex % 2 == 0)
			arg1.emit(myFirstStream, new Values(arg0.getValue(0)));
		else
			arg1.emit(mySecondStream, new Values(arg0.getValue(0)));

		hashIndex++;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer arg0) {
		arg0.declareStream(myFirstStream, new Fields("word"));
		arg0.declareStream(mySecondStream, new Fields("word"));
	}

}
