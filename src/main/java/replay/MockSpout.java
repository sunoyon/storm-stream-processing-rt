package replay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class MockSpout extends BaseRichSpout {

	private static final long serialVersionUID = -2651804367551922766L;
	SpoutOutputCollector _collector;
	private final String mockedData1 = "Hello-World"; 
	private final int mockedData1Count = 500; 
	private int myIndex = 0;
	public static Logger LOG = LoggerFactory.getLogger(MockSpout.class);

	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void nextTuple() {
		Utils.sleep(100);
		if( myIndex < mockedData1Count){
			_collector.emit(new Values(mockedData1), "spout-tuple-" + myIndex);
			myIndex++;
		}
	}
	
	@Override
	public void ack(Object msgId) {
	}
	
	@Override
	public void fail(Object msgId) {
		LOG.info("MockSpout failed object " + msgId);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
