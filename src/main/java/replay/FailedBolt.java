package replay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.FailedException;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class FailedBolt extends BaseBasicBolt {

	private static final long serialVersionUID = 781375166050495197L;
	public static Logger LOG = LoggerFactory.getLogger(FailedBolt.class);

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		LOG.info("In execute method of Failed-Bolt");
		throw new FailedException();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}
}
