package replay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class SleepBolt extends BaseBasicBolt {

	private static final long serialVersionUID = 5495019956682586411L;
	public static Logger LOG = LoggerFactory.getLogger(SleepBolt.class);

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		LOG.info("In execute method of Emitting-Bolt: " + Thread.currentThread().getId() + " message: " + tuple.getMessageId() + " tuple: " + tuple );
		try {
			Thread.sleep(40000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		collector.emit(new Values(tuple.getValue(0)));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
