package replay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class LoopBolt extends BaseBasicBolt {

	private static final long serialVersionUID = -3152418842263602211L;
	private static final int myLoopCount = 10;
	public static Logger LOG = LoggerFactory.getLogger(LoopBolt.class);

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		LOG.info("In execute method of Emitting-Bolt: " + Thread.currentThread().getId() + " message: " + input.getMessageId() + " tuple: " + input );
		for(int i = 0; i < myLoopCount; i++)
			collector.emit(new Values(input.getValue(0)));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
