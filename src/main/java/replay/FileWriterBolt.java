package replay;

import java.io.FileOutputStream;
import java.io.IOException;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class FileWriterBolt extends BaseBasicBolt {

	private static final long serialVersionUID = -4783945058032656742L;
	private String myFile;
	
	public FileWriterBolt(String myFile) {
		this.myFile = System.getProperty("user.home") + "/" + myFile + ".txt";
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		FileOutputStream anOutputStream;
		String result = (String)input.getValue(0) + "\n";
		try {
			anOutputStream = new FileOutputStream(myFile, true);
			anOutputStream.write(result.getBytes());
			anOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
	
}
